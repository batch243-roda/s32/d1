const http = require("http");
const port = 4000;

http
  .createServer((req, res) => {
    if (req.url === "/items" && req.method == "GET") {
      res.writeHead(200, { "Content-type": "text/plain" });

      res.end("Data retrieved from the database.");
    } else if (req.url === "/items" && req.method === "POST") {
      res.writeHead(200, { "Content-type": "text/plain" });

      res.end("Data sent to the database.");
    }
  })
  .listen(port);

console.log(`Server running at port ${port}`);
