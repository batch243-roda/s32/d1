const http = require("http");
const port = 4000;

const directory = [
  {
    name: "Jayson Roda",
    email: "roda@gmail.com",
  },
  {
    name: "Jayson Roda",
    email: "roda@gmail.com",
  },
];

http
  .createServer((req, res) => {
    if (req.url === "/users" && req.method === "GET") {
      res.writeHead(200, { "Content-type": "application/json" });

      res.write(JSON.stringify(directory));
      res.end();
    }

    if (req.url === "/users" && req.method === "POST") {
      // Declare and initialize a "requestBody" variable to an empty string
      // This will act as a placeholder for the data to be created later on
      let reqBody = "";

      // A stream is a sequence of data
      // Data is received from the client and is processed in the "data stream"
      // The information porvided from the request object enters a sequence called "data" the code below will be triggered
      // data step - this reads the "data" stream and processes it as the request body

      req.on("data", function (data) {
        // Assigns the data retrieved from the data stream to the requestBody
        reqBody += data;
      });
      // response end step - only runs after the request has completely been sent
      req.on("end", function () {
        console.log(typeof reqBody);

        reqBody = JSON.parse(reqBody);

        const newUser = {
          name: reqBody.name,
          email: reqBody.email,
        };

        directory.push(newUser);
        console.log(directory);

        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(JSON.stringify(newUser));
        res.end();
      });
    }
  })
  .listen(port);

console.log(`Server is running at port ${port}`);
